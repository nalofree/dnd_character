<?php

namespace App\Domain\Enumeration;

enum Gender: string
{
    case male = 'male';
    case female = 'female';
}
