<?php

namespace App\Domain\SkillCheck;

use App\Domain\Entity\Character;
use InvalidArgumentException;

class SkillChecker implements SkillCheckerInterface
{

    public function checkSkill(Character $character, string $ability, array $proficient): int
    {
        if (!in_array($ability, Character::AVAILABLE_ABILITY)) {
            throw new InvalidArgumentException("Неверное значение способности: $ability");
        }
        $modifier = $character->getAbilityModifier($character->getAbilityScores()->getValue());

        $skills = $character->getClass()->getSkills();
        $proficient = $skills->exists(function ($key, $value) use ($ability) {
            return $value->getAbility() === $ability;
        });

        if ($proficient) {
            $modifier += $character->getProficiencyBonus();
        }
        return $modifier;
    }
}
