<?php

namespace App\Domain\SkillCheck;

use App\Domain\Entity\Character;

interface SkillCheckerInterface
{
    public function checkSkill(Character $character, string $ability, array $proficient): int;
}
