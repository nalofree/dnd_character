<?php

namespace App\Domain\ValueObject\TelegramUser;

use App\Domain\Enumeration\Gender;
use InvalidArgumentException;

class Blank
{
    const DEFAULT_BLANK = [
        'name' => null,
        'level' => null,
        'gender' => null
    ];
    private array $blank;

    /**
     * @param array $blank
     */
    public function __construct(array $blank)
    {
//        $this->assertBlankArrayIsValid($blank);
        $this->blank = $blank;
    }

    private function assertBlankArrayIsValid(array $blank): void
    {
        if (getType($blank['name']) !== 'string' && strlen($blank['name']) < 1) {
            throw new InvalidArgumentException('Недопустимое имя');
        }
        if ($blank['level'] !== 1) {
            throw new InvalidArgumentException('Недопустимый уровень');
        }
        if (!($blank['gender'] instanceof Gender)) {
            throw new InvalidArgumentException('Недопустимый пол');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->blank;
    }
}
