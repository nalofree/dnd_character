<?php

namespace App\Domain\ValueObject;

use InvalidArgumentException;

class Description
{
    private string $description;

    /**
     * @param string $description
     */
    public function __construct(string $description)
    {
        $this->assertValueIsNotEmpty($description);
        $this->description = $description;
    }

    private function assertValueIsNotEmpty(string $description): void
    {
        if (empty($description)) {
            throw new InvalidArgumentException('Описание не может быть пустым');
        }
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->description;
    }
}
