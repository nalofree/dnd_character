<?php

namespace App\Domain\ValueObject\Race;

use InvalidArgumentException;

class LanguageList
{
    /**
     * @var array
     */
    private array $languages;

    /**
     * @param array $languages
     */
    public function __construct(array $languages)
    {
        $this->assertValueIsNotEmpty($languages);
        $this->languages = $languages;
    }

    /**
     * @param array $languages
     * @return void
     */
    private function assertValueIsNotEmpty(array $languages): void
    {

        if (empty($languages)) {
            throw new InvalidArgumentException('Список языков не может быть пустым');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->languages;
    }
}
