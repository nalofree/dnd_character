<?php

namespace App\Domain\ValueObject\Race;

use InvalidArgumentException;

class FeatureList
{
    /**
     * @var array
     */
    private array $features;

    /**
     * @param array $features
     */
    public function __construct(array $features)
    {
        $this->assertValueIsNotEmpty($features);
        $this->features = $features;
    }

    /**
     * @param array $features
     * @return void
     */
    private function assertValueIsNotEmpty(array $features): void
    {
        if (empty($features)) {
            throw new InvalidArgumentException('Список особенностей не может быть пустым');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->features;
    }
}
