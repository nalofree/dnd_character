<?php

namespace App\Domain\ValueObject\Race;

class Speed
{
    protected int $speed;

    /**
     * @param int $speed
     */
    public function __construct(int $speed)
    {
        $this->assertLevelGreaterThanZero($speed);
        $this->speed = $speed;
    }

    private function assertLevelGreaterThanZero(int $speed): void
    {
        if ($speed < 1) {
            throw new \InvalidArgumentException('Скорость персонажа не может быть меньше единицы.');
        }
    }

    public function getValue(): int
    {
        return $this->speed;
    }
}
