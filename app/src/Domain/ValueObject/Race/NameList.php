<?php

namespace App\Domain\ValueObject\Race;

use InvalidArgumentException;

class NameList
{
    /**
     * @var array
     */
    private array $names;

    /**
     * @param array $names
     */
    public function __construct(array $names)
    {
        $this->assertValueIsNotEmpty($names);
        $this->names = $names;
    }

    /**
     * @param array $names
     * @return void
     */
    private function assertValueIsNotEmpty(array $names): void
    {

        if (empty($names)) {
            throw new InvalidArgumentException('Список имен не может быть пустым');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->names;
    }
}
