<?php

namespace App\Domain\ValueObject\Race;

use App\Domain\Entity\Character;
use InvalidArgumentException;

class BonusAbility
{
    private array $ability;

    /**
     * @param array $ability
     */
    public function __construct(array $ability)
    {
        $this->assertValueGreaterThanNull($ability);
        $this->ability = $ability;
    }

    private function assertValueGreaterThanNull(array $ability): void //todo array
    {
        foreach ($ability as $key => $value) {
            if (!in_array($key, Character::AVAILABLE_ABILITY)) {
                throw new InvalidArgumentException('Недопустимая характеристика');
            }
            if ((int)$value < 1) {
                throw new InvalidArgumentException('Недопустимое значение характеристики');
            }
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->ability;
    }
}
