<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class Weapon
{
    private array $weapon;

    /**
     * @param array $weapon
     */
    public function __construct(array $weapon)
    {
        $this->assertWeaponsIsNotEmpty($weapon);
        $this->weapon = $weapon;
    }

    private function assertWeaponsIsNotEmpty(array $weapon): void
    {
        if (empty($weapon)) {
            throw new InvalidArgumentException('Допустимое оружие не могут быть пустым.');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->weapon;
    }
}
