<?php

namespace App\Domain\ValueObject\Klass;

use App\Domain\Entity\Character;
use InvalidArgumentException;

class AbilityPriority
{
    private array $abilityPriority;

    /**
     * @param array $abilityPriority
     */
    public function __construct(array $abilityPriority)
    {
        $this->assertTrowsIsNotValid($abilityPriority);
        $this->abilityPriority = $abilityPriority;
    }

    private function assertTrowsIsNotValid(array $abilityPriority): void
    {
        if ($abilityPriority != array_intersect($abilityPriority, Character::AVAILABLE_ABILITY)) {
            throw new InvalidArgumentException('Недопустимые характеристики');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->abilityPriority;
    }
}
