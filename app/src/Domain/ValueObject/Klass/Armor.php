<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class Armor
{
    private array $armor;

    /**
     * @param array $armor
     */
    public function __construct(array $armor)
    {
        $this->assertArmorIsNotEmpty($armor);
        $this->armor = $armor;
    }

    private function assertArmorIsNotEmpty(array $armor): void
    {
        if (empty($armor)) {
            throw new InvalidArgumentException('Допустимые доспехи не могут быть пусты.');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->armor;
    }
}
