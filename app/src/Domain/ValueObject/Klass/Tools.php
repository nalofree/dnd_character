<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class Tools
{
    private array $tools;

    /**
     * @param array $tools
     */
    public function __construct(array $tools)
    {
        $this->assertToolsIsNotEmpty($tools);
        $this->tools = $tools;
    }

    private function assertToolsIsNotEmpty(array $tools): void
    {
        if (empty($tools)) {
            throw new InvalidArgumentException('Допустимые инструменты не могут быть пусты.');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->tools;
    }
}
