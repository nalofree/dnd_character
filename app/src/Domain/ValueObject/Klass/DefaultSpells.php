<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class DefaultSpells
{
    private array $spells;

    /**
     * @param array $spells
     */
    public function __construct(array $spells)
    {
        $this->assertpellsIsNotEmpty($spells);
        $this->spells = $spells;
    }

    private function assertpellsIsNotEmpty(array $spells): void
    {
        if (empty($spells)) {
            throw new InvalidArgumentException('Список заговоров не может быть пуст');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->spells;
    }
}
