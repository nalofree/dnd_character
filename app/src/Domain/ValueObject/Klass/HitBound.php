<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class HitBound
{
    private string $bound;

    /**
     * @param string $bound
     */
    public function __construct(string $bound)
    {
        $this->assertHitBoundIsValid($bound);
        $this->bound = $bound;
    }

    private function assertHitBoundIsValid(string $bound): void
    {
        if (!preg_match('/\d[Kk](4|6|8|10|12|20)/', $bound)) {
            throw new InvalidArgumentException('Кость хитов имеет недопустимое значение.');
        }
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->bound;
    }
}
