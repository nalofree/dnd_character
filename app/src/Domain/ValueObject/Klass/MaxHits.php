<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class MaxHits
{
    private int $hits;

    /**
     * @param int $hits
     */
    public function __construct(int $hits)
    {
        $this->assertValueGreaterThanNull($hits);
        $this->hits = $hits;
    }

    private function assertValueGreaterThanNull(int $hits): void
    {
        if ($hits<1) {
            throw new InvalidArgumentException('Максимальное количество хитов не может быть меньше 1.');
        }
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->hits;
    }
}
