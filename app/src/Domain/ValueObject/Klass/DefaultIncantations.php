<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class DefaultIncantations
{
    private array $incantations;

    /**
     * @param array $incantations
     */
    public function __construct(array $incantations)
    {
        $this->assertpellsIsNotEmpty($incantations);
        $this->incantations = $incantations;
    }

    private function assertpellsIsNotEmpty(array $incantations): void
    {
        if (empty($incantations)) {
            throw new InvalidArgumentException('Список заклинаний не может быть пуст');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->incantations;
    }
}
