<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class Background
{
    private string $background;

    /**
     * @param string $background
     */
    public function __construct(string $background)
    {
        $this->assertValueIsNotEmpty($background);
        $this->background = $background;
    }

    private function assertValueIsNotEmpty(string $background): void
    {
        if (empty($background)) {
            throw new InvalidArgumentException('Предыстория не может быть пустой');
        }
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->background;
    }
}
