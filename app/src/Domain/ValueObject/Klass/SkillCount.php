<?php

namespace App\Domain\ValueObject\Klass;

use InvalidArgumentException;

class SkillCount
{
    private int $skillCount;

    /**
     * @param int $skillCount
     */
    public function __construct(int $skillCount)
    {
        $this->assertValueGreaterThanOne($skillCount);
        $this->skillCount = $skillCount;
    }

    private function assertValueGreaterThanOne(int $skillCount): void
    {
        if ($skillCount<1) {
            throw new InvalidArgumentException('Количество способностей на выбор не может быть меньше 1.');
        }
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->skillCount;
    }
}
