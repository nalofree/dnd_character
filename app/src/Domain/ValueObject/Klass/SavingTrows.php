<?php

namespace App\Domain\ValueObject\Klass;

use App\Domain\Entity\Character;
use InvalidArgumentException;

class SavingTrows
{
    private array $trows;

    /**
     * @param array $trows
     */
    public function __construct(array $trows)
    {
        $this->assertTrowsIsNotValid($trows);
        $this->trows = $trows;
    }

    private function assertTrowsIsNotValid(array $trows): void
    {
        if ($trows != array_intersect($trows, Character::AVAILABLE_ABILITY)) {
            throw new InvalidArgumentException('Недопустимые характеристики для спасброка');
        }
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->trows;
    }
}
