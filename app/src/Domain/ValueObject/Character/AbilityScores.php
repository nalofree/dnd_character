<?php

namespace App\Domain\ValueObject\Character;

use App\Domain\Entity\Character;
use InvalidArgumentException;

class AbilityScores
{
    private ?array $abilityArray = null;

    /**
     * @param array $abilityArray
     */
    public function __construct(array $abilityArray)
    {
        $this->assertAbilityArrayIsValid($abilityArray);
        $this->abilityArray = $abilityArray;
    }

    private function assertAbilityArrayIsValid(array $abilityArray): void
    {
        foreach ($this->abilityArray as $ability => $scores) {
            if ($scores < 1 || $scores > 20) {
                throw new InvalidArgumentException(
                    'Очков характеристики должно быть больше или равно 1, но не более 20.'
                );
            }
            if (!in_array($ability, Character::AVAILABLE_ABILITY)) {
                throw new InvalidArgumentException('Недопустимая характеристика');
            }
        }
    }

    /**
     * @return array|null
     */
    public function getValue(): ?array
    {
        return $this->abilityArray;
    }
}
