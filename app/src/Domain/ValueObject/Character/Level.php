<?php

namespace App\Domain\ValueObject\Character;

use InvalidArgumentException;

class Level
{
    protected int $level;

    /**
     * @param int $level
     */
    public function __construct(int $level)
    {
        $this->assertLevelGreaterThanZero($level);
        $this->level = $level;
    }

    private function assertLevelGreaterThanZero(int $level): void
    {
        if ($level < 1) {
            throw new InvalidArgumentException('Уровень не может быть больше единицы.');
        }
    }

    public function getLevel(): int
    {
        return $this->level;
    }
}
