<?php

namespace App\Domain\ValueObject;

use InvalidArgumentException;

class Name
{
    private string $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->assertValueIsNotEmpty($name);
        $this->name = $name;
    }

    private function assertValueIsNotEmpty(string $name): void
    {
        if (empty($name)) {
            throw new InvalidArgumentException('Имя не может быть пустым');
        }
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->name;
    }
}
