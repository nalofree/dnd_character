<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Skill;

interface SkillRepositoryInterface
{
    public function save(Skill $entity): void;

    public function delete(Skill $entity): void;
}
