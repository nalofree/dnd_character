<?php

namespace App\Domain\Repository;

use App\Domain\Entity\TelegramUser;

interface TelegramUserRepositoryInterface
{
    public function save(TelegramUser $entity): void;
    public function delete(TelegramUser $entity): void;
}
