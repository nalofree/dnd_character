<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Race;

interface RaceRepositoryInterface
{
    public function save(Race $entity): void;
    public function delete(Race $entity): void;
}
