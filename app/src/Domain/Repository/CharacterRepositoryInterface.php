<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Character;

interface CharacterRepositoryInterface
{
    public function save(Character $entity): void;
    public function delete(Character $entity): void;
}
