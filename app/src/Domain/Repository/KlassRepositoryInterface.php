<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Klass;

interface KlassRepositoryInterface
{
    public function save(Klass $entity): void;
    public function delete(Klass $entity): void;
}
