<?php

namespace App\Domain\Entity;

use App\Domain\ValueObject\TelegramUser\Blank;
use Doctrine\Common\Collections\Collection;

class TelegramUser
{
    private ?int $id = null;
    private string $telegram;

    private Blank $blank;

    private string $state; // standBy, inputHoldName

    private Collection $characters;

    /**
     * @param string $telegram
     */
    public function __construct(string $telegram, Blank $blank = new Blank(Blank::DEFAULT_BLANK))
    {
        $this->telegram = $telegram;
        $this->blank = $blank;
        $this->state = 'standBy';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTelegram(): string
    {
        return $this->telegram;
    }

    /**
     * @param string $telegram
     * @return TelegramUser
     */
    public function setTelegram(string $telegram): TelegramUser
    {
        $this->telegram = $telegram;
        return $this;
    }

    /**
     * @return Blank
     */
    public function getBlank(): Blank
    {
        return $this->blank;
    }

    /**
     * @param Blank $blank
     * @return TelegramUser
     */
    public function setBlank(Blank $blank): TelegramUser
    {
        $this->blank = $blank;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return TelegramUser
     */
    public function setState(string $state): TelegramUser
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    /**
     * @param Collection $characters
     * @return TelegramUser
     */
    public function setCharacters(Collection $characters): TelegramUser
    {
        $this->characters = $characters;
        return $this;
    }

    /**
     * @param Character $character
     * @return TelegramUser
     */
    public function addCharacters(Character $character): TelegramUser
    {
        $characters = $this->characters;
        $characters->add($character);
        $this->characters = $characters;
        return $this;
    }
}
