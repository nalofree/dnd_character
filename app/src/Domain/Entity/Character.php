<?php

namespace App\Domain\Entity;

use App\Domain\Enumeration\Gender;
use App\Domain\ValueObject\Character\AbilityScores;
use App\Domain\ValueObject\Character\Level;
use App\Domain\ValueObject\Name;

class Character
{
    const AVAILABLE_ABILITY = [
        "strength",
        "dexterity",
        "constitution",
        "intelligence",
        "wisdom",
        "charisma"
    ];

    private ?int $id = null;
    private Name $name;
    private ?Race $race = null;
    private ?Klass $class = null;
    private Level $level;
    private Gender $gender;
    private ?AbilityScores $abilityScores = null;

    /**
     * @param Name $name
     * @param Level $level
     * @param Gender $gender
     */
    public function __construct(
        Name $name,
        Level $level,
        Gender $gender
    ) {
        $this->name = $name;
        $this->level = $level;
        $this->gender = $gender;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @param Name $name
     * @return Character
     */
    public function setName(Name $name): Character
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Race|null
     */
    public function getRace(): ?Race
    {
        return $this->race;
    }

    /**
     * @param Race $race
     * @return Character
     */
    public function setRace(Race $race): Character
    {
        $this->race = $race;
        return $this;
    }

    /**
     * @return Klass|null
     */
    public function getClass(): ?Klass
    {
        return $this->class;
    }

    /**
     * @param Klass $class
     * @return Character
     */
    public function setClass(Klass $class): Character
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return Level
     */
    public function getLevel(): Level
    {
        return $this->level;
    }

    /**
     * @param Level $level
     * @return Character
     */
    public function setLevel(Level $level): Character
    {
        $this->level = $level;
        return $this;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function setGender(Gender $gender): Character
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return AbilityScores|null
     */
    public function getAbilityScores(): ?AbilityScores
    {
        return $this->abilityScores;
    }

    /**
     * @param AbilityScores $abilityScores
     * @return Character
     */
    public function setAbilityScores(AbilityScores $abilityScores): Character
    {
        $this->abilityScores = $abilityScores;
        return $this;
    }

    public function getAbilityScoreByName($abilityName): ?int
    {
        if (!$this->abilityScores) {
            return null;
        }
        $abilityArray = $this->abilityScores->getValue();
        if (isset($abilityArray[$abilityName])) {
            return $abilityArray[$abilityName];
        }
        return null;
    }

    // метод для вычисления модификатора способности
    public function getAbilityModifier($abilityScore): int
    {
        return (int) floor(($abilityScore - 10) / 2);
    }

    // метод для вычисления бонуса мастерства
    public function getProficiencyBonus(): int
    {
        $level = $this->level->getLevel();
        return match (true) {
            $level >= 5 => 3,
            $level >= 9 => 4,
            $level >= 13 => 5,
            $level >= 17 => 6,
            default => 2
        };
    }
}
