<?php

namespace App\Domain\Entity;

use App\Domain\ValueObject\Description;
use App\Domain\ValueObject\Name;
use App\Domain\ValueObject\Race\BonusAbility;
use App\Domain\ValueObject\Race\FeatureList;
use App\Domain\ValueObject\Race\LanguageList;
use App\Domain\ValueObject\Race\NameList;
use App\Domain\ValueObject\Race\Speed;

class Race
{
    private ?int $id = null;
    private Name $name;
    private Description $description;

    private NameList $male_names;
    private NameList $female_names;
    private LanguageList $languages;
    private FeatureList $features;

    private BonusAbility $bonusAbility;
    private Speed $speed;

    /**
     * @param Name $name
     * @param Description $description
     * @param NameList $male_names
     * @param NameList $female_names
     * @param LanguageList $languages
     * @param FeatureList $features
     * @param BonusAbility $bonusAbility
     * @param Speed $speed
     */
    public function __construct(
        Name $name,
        Description $description,
        NameList $male_names,
        NameList $female_names,
        LanguageList $languages,
        FeatureList $features,
        BonusAbility $bonusAbility,
        Speed $speed
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->male_names = $male_names;
        $this->female_names = $female_names;
        $this->languages = $languages;
        $this->features = $features;
        $this->bonusAbility = $bonusAbility;
        $this->speed = $speed;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function setName(Name $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function setDescription(Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Speed
     */
    public function getSpeed(): Speed
    {
        return $this->speed;
    }

    /**
     * @param Speed $speed
     * @return Race
     */
    public function setSpeed(Speed $speed): Race
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return NameList
     */
    public function getMaleNames(): NameList
    {
        return $this->male_names;
    }

    /**
     * @param NameList $male_names
     * @return Race
     */
    public function setMaleNames(NameList $male_names): Race
    {
        $this->male_names = $male_names;
        return $this;
    }

    /**
     * @return NameList
     */
    public function getFemaleNames(): NameList
    {
        return $this->female_names;
    }

    /**
     * @param NameList $female_names
     * @return Race
     */
    public function setFemaleNames(NameList $female_names): Race
    {
        $this->female_names = $female_names;
        return $this;
    }

    /**
     * @return LanguageList
     */
    public function getLanguages(): LanguageList
    {
        return $this->languages;
    }

    /**
     * @param LanguageList $languages
     * @return Race
     */
    public function setLanguages(LanguageList $languages): Race
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @return FeatureList
     */
    public function getFeatures(): FeatureList
    {
        return $this->features;
    }

    /**
     * @param FeatureList $features
     * @return Race
     */
    public function setFeatures(FeatureList $features): Race
    {
        $this->features = $features;
        return $this;
    }

    /**
     * @return BonusAbility
     */
    public function getBonusAbility(): BonusAbility
    {
        return $this->bonusAbility;
    }

    /**
     * @param BonusAbility $bonusAbility
     * @return Race
     */
    public function setBonusAbility(BonusAbility $bonusAbility): Race
    {
        $this->bonusAbility = $bonusAbility;
        return $this;
    }
}
