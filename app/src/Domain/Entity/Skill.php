<?php

namespace App\Domain\Entity;

use App\Domain\ValueObject\Name;
use Doctrine\Common\Collections\Collection;

class Skill
{
    private ?int $id = null;

    public function __construct(
        private readonly Name $name,
        private readonly string $ability
    ) {
        $this->assertIsAbilityAllowed($ability);
    }

    private function assertIsAbilityAllowed($ability): void
    {
        if (!in_array($ability, Character::AVAILABLE_ABILITY)) {
            throw new \InvalidArgumentException('Недопустимая характеристика');
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAbility(): string
    {
        return $this->ability;
    }
}
