<?php

namespace App\Domain\Entity;

use App\Domain\ValueObject\Description;
use App\Domain\ValueObject\Klass\AbilityPriority;
use App\Domain\ValueObject\Klass\Armor;
use App\Domain\ValueObject\Klass\Background;
use App\Domain\ValueObject\Klass\DefaultIncantations;
use App\Domain\ValueObject\Klass\DefaultSpells;
use App\Domain\ValueObject\Klass\HitBound;
use App\Domain\ValueObject\Klass\MaxHits;
use App\Domain\ValueObject\Klass\SavingTrows;
use App\Domain\ValueObject\Klass\SkillCount;
use App\Domain\ValueObject\Klass\Tools;
use App\Domain\ValueObject\Klass\Weapon;
use App\Domain\ValueObject\Name;
use Doctrine\Common\Collections\Collection;

class Klass
{
    private ?int $id = null;
    private Name $name;
    private Description $description;
    private HitBound $hitBound;
    private MaxHits $maxHits;
    private Armor $armor;
    private Weapon $weapon;
    private Tools $tools;
    private SavingTrows $savingTrows;
    private Collection $skills;
    private SkillCount $skillCount;
    private DefaultSpells $spells;
    private DefaultIncantations $incantations;
    private AbilityPriority $abilityPriority;
    private Background $background;

    /**
     * @param Name $name
     * @param Description $description
     * @param HitBound $hitBound
     * @param MaxHits $maxHits
     * @param Armor $armor
     * @param Weapon $weapon
     * @param Tools $tools
     * @param SavingTrows $savingTrows
     * @param Collection $skills
     * @param SkillCount $skillCount
     * @param DefaultSpells $spells
     * @param DefaultIncantations $incantations
     * @param AbilityPriority $abilityPriority
     * @param Background $background
     */
    public function __construct(
        Name $name,
        Description $description,
        HitBound $hitBound,
        MaxHits $maxHits,
        Armor $armor,
        Weapon $weapon,
        Tools $tools,
        SavingTrows $savingTrows,
        Collection $skills,
        SkillCount $skillCount,
        DefaultSpells $spells,
        DefaultIncantations $incantations,
        AbilityPriority $abilityPriority,
        Background $background
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->hitBound = $hitBound;
        $this->maxHits = $maxHits;
        $this->armor = $armor;
        $this->weapon = $weapon;
        $this->tools = $tools;
        $this->savingTrows = $savingTrows;
        $this->skills = $skills;
        $this->skillCount = $skillCount;
        $this->spells = $spells;
        $this->incantations = $incantations;
        $this->abilityPriority = $abilityPriority;
        $this->background = $background;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function setName(Name $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): Description
    {
        return $this->description;
    }

    public function setDescription(Description $description): void
    {
        $this->description = $description;
    }

    /**
     * @return HitBound
     */
    public function getHitBound(): HitBound
    {
        return $this->hitBound;
    }

    /**
     * @param HitBound $hitBound
     * @return Klass
     */
    public function setHitBound(HitBound $hitBound): Klass
    {
        $this->hitBound = $hitBound;
        return $this;
    }

    /**
     * @return MaxHits
     */
    public function getMaxHits(): MaxHits
    {
        return $this->maxHits;
    }

    /**
     * @param MaxHits $maxHits
     * @return Klass
     */
    public function setMaxHits(MaxHits $maxHits): Klass
    {
        $this->maxHits = $maxHits;
        return $this;
    }

    /**
     * @return Armor
     */
    public function getArmor(): Armor
    {
        return $this->armor;
    }

    /**
     * @param Armor $armor
     * @return Klass
     */
    public function setArmor(Armor $armor): Klass
    {
        $this->armor = $armor;
        return $this;
    }

    /**
     * @return Weapon
     */
    public function getWeapon(): Weapon
    {
        return $this->weapon;
    }

    /**
     * @param Weapon $weapon
     * @return Klass
     */
    public function setWeapon(Weapon $weapon): Klass
    {
        $this->weapon = $weapon;
        return $this;
    }

    /**
     * @return Tools
     */
    public function getTools(): Tools
    {
        return $this->tools;
    }

    /**
     * @param Tools $tools
     * @return Klass
     */
    public function setTools(Tools $tools): Klass
    {
        $this->tools = $tools;
        return $this;
    }

    /**
     * @return SavingTrows
     */
    public function getSavingTrows(): SavingTrows
    {
        return $this->savingTrows;
    }

    /**
     * @param SavingTrows $savingTrows
     * @return Klass
     */
    public function setSavingTrows(SavingTrows $savingTrows): Klass
    {
        $this->savingTrows = $savingTrows;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @param Collection $skills
     * @return Klass
     */
    public function setSkills(Collection $skills): Klass
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return SkillCount
     */
    public function getSkillCount(): SkillCount
    {
        return $this->skillCount;
    }

    /**
     * @param SkillCount $skillCount
     * @return Klass
     */
    public function setSkillCount(SkillCount $skillCount): Klass
    {
        $this->skillCount = $skillCount;
        return $this;
    }

    /**
     * @return DefaultSpells
     */
    public function getSpells(): DefaultSpells
    {
        return $this->spells;
    }

    /**
     * @param DefaultSpells $spells
     * @return Klass
     */
    public function setSpells(DefaultSpells $spells): Klass
    {
        $this->spells = $spells;
        return $this;
    }

    /**
     * @return DefaultIncantations
     */
    public function getIncantations(): DefaultIncantations
    {
        return $this->incantations;
    }

    /**
     * @param DefaultIncantations $incantations
     * @return Klass
     */
    public function setIncantations(DefaultIncantations $incantations): Klass
    {
        $this->incantations = $incantations;
        return $this;
    }

    /**
     * @return AbilityPriority
     */
    public function getAbilityPriority(): AbilityPriority
    {
        return $this->abilityPriority;
    }

    /**
     * @param AbilityPriority $abilityPriority
     * @return Klass
     */
    public function setAbilityPriority(AbilityPriority $abilityPriority): Klass
    {
        $this->abilityPriority = $abilityPriority;
        return $this;
    }

    /**
     * @return Background
     */
    public function getBackground(): Background
    {
        return $this->background;
    }

    /**
     * @param Background $background
     * @return Klass
     */
    public function setBackground(Background $background): Klass
    {
        $this->background = $background;
        return $this;
    }
}
