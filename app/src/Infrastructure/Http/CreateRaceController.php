<?php

namespace App\Infrastructure\Http;

use App\Application\UseCase\CreateRaceUseCase;
use App\Application\UseCase\DTO\Request\CreateRaceRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

class CreateRaceController extends AbstractController
{
    public function __construct(private readonly CreateRaceUseCase $useCase)
    {
    }

    #[Route('/api/race', methods: ['POST'])]
    public function __invoke(#[MapRequestPayload] CreateRaceRequest $requestDto, Request $request): Response
    {
        try {
            $responseDto = ($this->useCase)($requestDto);
            return new JsonResponse(["data" => $responseDto, "status" => 200], 200);
        } catch (Throwable $e) {
            $statusCode = 400;
            return new JsonResponse(["message" => $e->getMessage(), "status" => $statusCode], $statusCode);
        }
    }
}
