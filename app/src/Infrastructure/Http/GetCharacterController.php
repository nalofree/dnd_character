<?php

namespace App\Infrastructure\Http;

use App\Application\UseCase\DTO\Request\GetCharacterRequest;
use App\Application\UseCase\GetCharacterUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

class GetCharacterController extends AbstractController
{
    public function __construct(
        private readonly GetCharacterUseCase $useCase
    ) {
    }

    #[Route('/api/character/{id}', name: 'character_details', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function __invoke(int $id): Response
    {
        $requestDto = new GetCharacterRequest($id);
        try {
            $responseDto = ($this->useCase)($requestDto);
            return new JsonResponse(["data" => $responseDto, "status" => 200], 200);
        } catch (Throwable $e) {
            return new JsonResponse(["message" => $e->getMessage(), "status" => 404], 404);
        }
    }
}
