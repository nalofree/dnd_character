<?php

namespace App\Infrastructure\Http;

use App\Application\UseCase\CreateSkillUseCase;
use App\Application\UseCase\DTO\Request\CreateSkillRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

class CreateSkillController extends AbstractController
{
    public function __construct(private readonly CreateSkillUseCase $useCase)
    {
    }

    #[Route('/api/skill', methods: ['POST'])]
    public function __invoke(#[MapRequestPayload] CreateSkillRequest $requestDto, Request $request): Response
    {
        try {
            $responseDto = ($this->useCase)($requestDto);
            return new JsonResponse(["data" => $responseDto, "status" => 200], 200);
        } catch (Throwable $e) {
            $statusCode = 400;
            return new JsonResponse(["message" => $e->getMessage(), "status" => $statusCode], $statusCode);
        }
    }
}
