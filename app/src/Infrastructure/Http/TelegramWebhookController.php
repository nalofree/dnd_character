<?php

namespace App\Infrastructure\Http;

use A\B;
use App\Application\Command\Telegram\CharacterCommand;
use App\Application\Command\Telegram\CharacterListCommand;
use App\Application\Command\Telegram\ClassCommand;
use App\Application\Command\Telegram\CountinueCommand;
use App\Application\Command\Telegram\GenderCommand;
use App\Application\Command\Telegram\InvalidCommand;
use App\Application\Command\Telegram\NewCommand;
use App\Application\Command\Telegram\RaceCommand;
use App\Application\Command\Telegram\StartCommand;
use App\Domain\Entity\Character;
use App\Domain\Entity\TelegramUser;
use App\Domain\Enumeration\Gender;
use App\Domain\ValueObject\Character\Level;
use App\Domain\ValueObject\Name;
use App\Domain\ValueObject\TelegramUser\Blank;
use App\Infrastructure\Repository\CharacterRepository;
use App\Infrastructure\Repository\KlassRepository;
use App\Infrastructure\Repository\RaceRepository;
use App\Infrastructure\Repository\TelegramUserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TelegramWebhookController extends AbstractController
{
    #[Route('/api/pub/telegram/webhook/', name: 'race_list', methods: ['POST'])]
    public function __invoke(
        Request $request,
        LoggerInterface $logger,
        SerializerInterface $serializer,
        KernelInterface $kernel,
        TelegramUserRepository $telegramUserRepository,
        CharacterRepository $characterRepository,
        RaceRepository $raceRepository,
        KlassRepository $classRepository,
    ): Response {
        $logger->debug((string) $request->getContent() ?? '{}', ['telegram']);
        try {
//            /** @var UpdateRequest $updateRequest */
//            $updateRequest = $serializer->deserialize($request->getContent(), UpdateRequest::class, 'json');
            $updateRequest = json_decode($request->getContent(), true);

            $telegramUser = $telegramUserRepository->findOneBy(
                ['telegram'=> isset($updateRequest['message'])
                    ? $updateRequest['message']['chat']['id']
                    : $updateRequest['callback_query']['message']['chat']['id']]
            );

            $application = new Application($kernel);
            $application->setAutoExit(false);
            if (isset($updateRequest['message'])) {
                switch ($updateRequest['message']['text'])
                {
                    case '/start':
                        // Если этот диалог пуст, то приветсвуем и предлагаем создать перса
                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser(
                                $updateRequest['message']['chat']['id'],
                                new Blank(Blank::DEFAULT_BLANK),
                                'standBy'
                            );
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id']
                            ]);
                            // Если диалог не пуст, то предлагаем посмотреть список персов или создать нового
                        } else {
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:continue',
                                StartCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id']
                            ]);
                        }

                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','/start']);
                        break;
                    case '/new':
                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser($updateRequest['message']['chat']['id']);
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id']
                            ]);
                        } else {
                            $blank = $telegramUser->getBlank()->getValue();

                            if (empty($blank['name']) || empty($blank['level']) || empty($blank['gender'])) {
                                $input = new ArrayInput([
                                    'command' => 'app:telegram:command:new',
                                    NewCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id'],
                                    NewCommand::ARG_BLANK => json_encode($blank)
                                ]);
                            }
                        }



                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','/new']);
                        break;
                    default:
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['message']['chat']['id']]
                        );

                        $input = new ArrayInput([
                            'command' => 'app:telegram:command:invalid',
                            InvalidCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id'],
                        ]);

                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser($updateRequest['message']['chat']['id']);
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id']
                            ]);
                        } else {
                            $state = $telegramUser->getState();
                            if ($state === 'inputHoldName') {// Если стейт ожидания имени, то будем его проверять
                                $name = $updateRequest['message']['text'];
                                if (strlen($name) > 0) {
                                    $blank = $telegramUser->getBlank()->getValue();
                                    $blank['name'] = $name;
                                    $telegramUser->setBlank(new Blank($blank));
                                    $input = new ArrayInput([
                                        'command' => 'app:telegram:command:new',
                                        NewCommand::ARG_CHAT_ID => $updateRequest['message']['chat']['id'],
                                        NewCommand::ARG_BLANK => json_encode($telegramUser->getBlank()->getValue())
                                    ]);
                                    $telegramUser->setState('standBy');
                                    $telegramUserRepository->save($telegramUser);
                                }
                            }
                        }
                        $telegramUser->setState('standBy');
                        $telegramUserRepository->save($telegramUser);
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                }
            }

            if (isset($updateRequest['callback_query'])) {
                //self::telegram('editMessageReplyMarkup?chat_id=' . self::CHAT_ID . '&message_id='
                //        . $result->callback_query->message->message_id . '&reply_markup={}');
                switch ($updateRequest['callback_query']['data'])
                {
//                    case '/start':
//                        $input = new ArrayInput([
//                            'command' => 'app:telegram:command:start',
//                            StartCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id']
//                        ]);
//                        $output = new BufferedOutput();
//                        $application->run($input, $output);
//                        $content = $output->fetch();
//                        $logger->debug($content, ['telegram','callback_query']);
//                        break;
                    case '/new':
                        $input = new ArrayInput([
                            'command' => 'app:telegram:command:new',
                            NewCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                            NewCommand::ARG_BLANK => json_encode($telegramUser->getBlank()->getValue()),
                            NewCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                        ]);
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                        break;
                    case 'setBlankName':
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser($updateRequest['callback_query']['message']['chat']['id']);
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id']
                            ]);
                        } else {
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:name',
                                StartCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id']
                            ]);
                            $telegramUser->setState('inputHoldName');
                            $telegramUserRepository->save($telegramUser);
                        }
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                        break;
                    case 'setBlankGender':
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser($updateRequest['callback_query']['message']['chat']['id']);
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id']
                            ]);
                        } else {
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:gender',
                                GenderCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                GenderCommand::ARG_BLANK => json_encode($telegramUser->getBlank()->getValue()),
                                GenderCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                            ]);
                            $telegramUser->setState('standBy');
                            $telegramUserRepository->save($telegramUser);
                        }
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                        break;
                    case 'setBlankLevel':
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        if (empty($telegramUser)) {
                            $telegramUser = new TelegramUser($updateRequest['callback_query']['message']['chat']['id']);
                            $telegramUserRepository->save($telegramUser);
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:start',
                                StartCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id']
                            ]);
                        } else {
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:level',
                                GenderCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                GenderCommand::ARG_BLANK => json_encode($telegramUser->getBlank()->getValue()),
                                GenderCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                            ]);
                            $telegramUser->setState('standBy');
                            $telegramUserRepository->save($telegramUser);
                        }
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                        break;
                    case 'saveBlank': // Сохраняем болванку в перса
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        $blank = $telegramUser->getBlank()->getValue();
                        $character = new Character(
                            new Name($blank['name']),
                            new Level($blank['level']),
                            Gender::tryFrom($blank['gender']),
                        );
                        $characterRepository->save($character);
                        $telegramUser->addCharacters($character);
                        $telegramUser->setBlank(new Blank(Blank::DEFAULT_BLANK));
                        $telegramUserRepository->save($telegramUser);

                        $characterArray = [
                            'id' => $character->getId(),
                            'name' => $character->getName()->getValue(),
                            'level' => $character->getLevel()->getLevel(),
//                            'abilities' => $character->getAbilityScores()->getValue()
                        ];

                        $input = new ArrayInput([
                            'command' => 'app:telegram:command:character',
                            CharacterCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                            CharacterCommand::ARG_CHARACTER => json_encode($characterArray),
                            CharacterCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                        ]);
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);

                        // Теперь покажем нащего перса как перса
                        break;
                    case 'CharacterList':
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        $characters = $telegramUser->getCharacters();
                        $charactersArray = [];
                        foreach ($characters as $character) {
                            $charactersArray[] = [
                                'id' => $character->getId(),
                                'name' => $character->getName()->getValue(),
                                'level' => $character->getLevel()->getLevel()
                            ];
                        }

                        $input = new ArrayInput([
                            'command' => 'app:telegram:command:characterlist',
                            CharacterListCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                            CharacterListCommand::ARG_CHARACTER_LIST => json_encode($charactersArray),
                            CharacterListCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                        ]);
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);

                        break;
                    default:
                        $telegramUser = $telegramUserRepository->findOneBy(
                            ['telegram'=>$updateRequest['callback_query']['message']['chat']['id']]
                        );
                        $data = json_decode($updateRequest['callback_query']['data'], true);
                        if (isset($data['blank'])) {
                            // Изменяем болванку
                            $blank = $telegramUser->getBlank()->getValue();
                            foreach ($data['blank'] as $key => $value) {
                                $blank[$key] = $value;
                            }
                            $telegramUser->setBlank(new Blank($blank));
                            $telegramUserRepository->save($telegramUser);

                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:new',
                                NewCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                NewCommand::ARG_BLANK => json_encode($telegramUser->getBlank()->getValue()),
                                NewCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                            ]);
                            $output = new BufferedOutput();
                            $application->run($input, $output);
                            $content = $output->fetch();
                            $logger->debug($content, ['telegram','callback_query']);
                            return new JsonResponse([]);
                        }

                        if (isset($data['character'])) {
                            $character = $characterRepository->findOneBy(['id'=>$data['character']['character_id']]);
                            if (isset($data['character']['race'])) {
                                $race = $raceRepository->find($data['character']['race']);
                                $character->setRace($race);
                                $characterRepository->save($character);
                            }
                            if (isset($data['character']['class'])) {
                                $class = $classRepository->find($data['character']['class']);
                                $character->setClass($class);
                                $characterRepository->save($character);
                            }
                            if (isset($data['character']['ability'])) {
                                // ability comand
                            }
                            // Изменяем персонажа
//                            $blank = $telegramUser->getBlank()->getValue();
//                            foreach ($data['character'] as $key => $value) {
//                                $blank[$key] = $value;
//                            }
//                            $telegramUser->setBlank(new Blank($blank));
//                            $telegramUserRepository->save($telegramUser);

                            $characterArray = [
                                'id' => $character->getId(),
                                'name' => $character->getName()->getValue(),
                                'level' => $character->getLevel()->getLevel(),
                                'abilities' => $character->getAbilityScores()?->getValue(),
                                'race' => $character->getRace()?->getName()->getValue(),
                                'class' => $character->getClass()?->getName()->getValue(),
                            ];

                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:character',
                                CharacterCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                CharacterCommand::ARG_CHARACTER => json_encode($characterArray),
                                CharacterCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                            ]);
                            $output = new BufferedOutput();
                            $application->run($input, $output);
                            $content = $output->fetch();
                            $logger->debug($content, ['telegram','callback_query']);
                            return new JsonResponse([]);
                        }

                        if (isset($data['setCharacterRace'])) {
                            $raceList = $raceRepository->findAll();
                            $raceListArray = [];
                            foreach ($raceList as $race) {
                                $raceListArray[] = [
                                    'id' => $race->getId(),
                                    'name' => $race->getName()->getValue(),
                                ];
                            }
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:race',
                                RaceCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                RaceCommand::ARG_CHARACTER_ID => $data['setCharacterRace']['characterId'],
                                RaceCommand::ARG_RACE_LIST => json_encode($raceListArray),
                                RaceCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id'],

                            ]);
                            $output = new BufferedOutput();
                            $application->run($input, $output);
                            $content = $output->fetch();
                            $logger->debug($content, ['telegram','callback_query']);
                            return new JsonResponse([]);
                        }

                        if (isset($data['setCharacterClass'])) {
                            $classList = $classRepository->findAll();
                            $classListArray = [];
                            foreach ($classList as $class) {
                                $classListArray[] = [
                                    'id' => $class->getId(),
                                    'name' => $class->getName()->getValue(),
                                ];
                            }
                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:class',
                                ClassCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                ClassCommand::ARG_CHARACTER_ID => $data['setCharacterClass']['characterId'],
                                ClassCommand::ARG_CLASS_LIST => json_encode($classListArray),
                                ClassCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id'],

                            ]);
                            $output = new BufferedOutput();
                            $application->run($input, $output);
                            $content = $output->fetch();
                            $logger->debug($content, ['telegram','callback_query']);
                            return new JsonResponse([]);
                        }

//                        if (isset($data['setCharacterAbility'])) {
//                            $raceList = $raceRepository->findAll();
//                            $raceListArray = [];
//                            foreach ($raceList as $race) {
//                                $raceListArray[] = [
//                                    'id' => $race->getId(),
//                                    'name' => $race->getName()->getValue(),
//                                ];
//                            }
//                            $input = new ArrayInput([
//                                'command' => 'app:telegram:command:race',
//                                RaceCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
//                                RaceCommand::ARG_CHARACTER_ID => $data['setCharacterRace']['characterId'],
//                                RaceCommand::ARG_RACE_LIST => json_encode($raceListArray),
//                                RaceCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id'],
//
//                            ]);
//                            $output = new BufferedOutput();
//                            $application->run($input, $output);
//                            $content = $output->fetch();
//                            $logger->debug($content, ['telegram','callback_query']);
//                            return new JsonResponse([]);
//                        }

                        if (isset($data['CharacterView'])) {
                            $character = $characterRepository->findOneBy(['id'=>$data['CharacterView']['id']]);
                            $characterArray = [
                                'id' => $character->getId(),
                                'name' => $character->getName()->getValue(),
                                'level' => $character->getLevel()->getLevel(),
                                'abilities' => $character->getAbilityScores()?->getValue(),
                                'race' => $character->getRace()?->getName()->getValue(),
                                'class' => $character->getClass()?->getName()->getValue(),
                            ];

                            $input = new ArrayInput([
                                'command' => 'app:telegram:command:character',
                                CharacterCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                                CharacterCommand::ARG_CHARACTER => json_encode($characterArray),
                                CharacterCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                            ]);
                            $output = new BufferedOutput();
                            $application->run($input, $output);
                            $content = $output->fetch();
                            $logger->debug($content, ['telegram','callback_query']);
                            return new JsonResponse([]);
                        }

                        $input = new ArrayInput([
                            'command' => 'app:telegram:command:continue',
                            CountinueCommand::ARG_CHAT_ID => $updateRequest['callback_query']['message']['chat']['id'],
                            CountinueCommand::MESSAGE_ID => $updateRequest['callback_query']['message']['message_id']
                        ]);
                        $output = new BufferedOutput();
                        $application->run($input, $output);
                        $content = $output->fetch();
                        $logger->debug($content, ['telegram','callback_query']);
                }
            }
        } catch (\Throwable|\Error $e) {
            $logger->critical($e->getMessage().'::'.$e->getFile().':'.$e->getLine(), ['telegram_bot_controller']);
        }
        return new JsonResponse([]);
    }
}
