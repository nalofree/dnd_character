<?php

namespace App\Infrastructure\Http;

use App\Application\UseCase\DTO\Request\GetKlassRequest;
use App\Application\UseCase\GetKlassUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

class GetKlassController extends AbstractController
{
    public function __construct(
        private readonly GetKlassUseCase $useCase
    ) {
    }

    #[Route('/api/class/{id}', name: 'class_details', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function __invoke(int $id): Response
    {
        $requestDto = new GetKlassRequest($id);
        try {
            $responseDto = ($this->useCase)($requestDto);
            return new JsonResponse(["data" => $responseDto, "status" => 200], 200);
        } catch (Throwable $e) {
            return new JsonResponse(["message" => $e->getMessage(), "status" => 404], 404);
        }
    }
}
