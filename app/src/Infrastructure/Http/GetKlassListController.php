<?php

namespace App\Infrastructure\Http;

use App\Application\UseCase\GetKlassListUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Throwable;

class GetKlassListController extends AbstractController
{
    public function __construct(
        private readonly GetKlassListUseCase $useCase
    ) {
    }

    #[Route('/api/class/list', name: 'class_list', methods: ['GET'])]
    public function __invoke(): Response
    {
        try {
            $responseDto = ($this->useCase)();
            return new JsonResponse(["data" => $responseDto, "status" => 200], 200);
        } catch (Throwable $e) {
            return new JsonResponse(["message" => $e->getMessage(), "status" => 404], 404);
        }
    }
}
