<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Skill;
use App\Domain\Repository\SkillRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Skill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skill[] findAll()
 * @method Skill[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRepository extends ServiceEntityRepository implements SkillRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    /**
     * @param Skill $entity
     * @return void
     */
    public function save(Skill $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param Skill $entity
     * @return void
     */
    public function delete(Skill $entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }
}
