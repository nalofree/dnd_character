<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Character;
use App\Domain\Repository\CharacterRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Character|null find($id, $lockMode = null, $lockVersion = null)
 * @method Character|null findOneBy(array $criteria, array $orderBy = null)
 * @method Character[] findAll()
 * @method Character[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CharacterRepository extends ServiceEntityRepository implements CharacterRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Character::class);
    }

    /**
     * @param Character $entity
     * @return void
     */
    public function save(Character $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param Character $entity
     * @return void
     */
    public function delete(Character $entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }
}
