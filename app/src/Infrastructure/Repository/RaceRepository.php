<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Race;
use App\Domain\Repository\RaceRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Race|null find($id, $lockMode = null, $lockVersion = null)
 * @method Race|null findOneBy(array $criteria, array $orderBy = null)
 * @method Race[] findAll()
 * @method Race[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RaceRepository extends ServiceEntityRepository implements RaceRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Race::class);
    }

    /**
     * @param Race $entity
     * @return void
     */
    public function save(Race $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param Race $entity
     * @return void
     */
    public function delete(Race $entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }
}
