<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\TelegramUser;
use App\Domain\Repository\TelegramUserRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TelegramUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TelegramUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TelegramUser[] findAll()
 * @method TelegramUser[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TelegramUserRepository extends ServiceEntityRepository implements TelegramUserRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TelegramUser::class);
    }

    /**
     * @param TelegramUser $entity
     * @return void
     */
    public function save(TelegramUser $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param TelegramUser $entity
     * @return void
     */
    public function delete(TelegramUser $entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }
}
