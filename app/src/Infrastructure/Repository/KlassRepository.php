<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Klass;
use App\Domain\Repository\KlassRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Klass|null find($id, $lockMode = null, $lockVersion = null)
 * @method Klass|null findOneBy(array $criteria, array $orderBy = null)
 * @method Klass[] findAll()
 * @method Klass[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KlassRepository extends ServiceEntityRepository implements KlassRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Klass::class);
    }

    /**
     * @param Klass $entity
     * @return void
     */
    public function save(Klass $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    /**
     * @param Klass $entity
     * @return void
     */
    public function delete(Klass $entity): void
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }
}
