<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:continue',
    description: '/continue'
)]
class CountinueCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_ID = 'messageId';
    const MESSAGE_START = 'Привет опять! Мы уже виделись!';
    private array $buttons = [
        'inline_keyboard' => [
            [
                [
                    'text' => 'Создать персонажа',
                    'callback_data' => '/new'
                ]
            ],
            [
                [
                    'text' => 'Просмотреть персонажей',
                    'callback_data' => 'CharacterList'
                ]
            ]
        ]
    ];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ')
            ->AddArgument(self::MESSAGE_ID, InputArgument::OPTIONAL, 'Telegram messageID ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);
        $messageId = $input->getArgument(self::MESSAGE_ID);

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            if ($messageId) {
                $this->sender->editMessageText(
                    $chatId,
                    $messageId,
                    self::MESSAGE_START,
                    $this->buttons
                );
            } else {
                $this->sender->sendMessage(
                    $chatId,
                    self::MESSAGE_START,
                    $this->buttons
                );
            }
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
