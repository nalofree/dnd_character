<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:characterlist',
    description: 'Character list command'
)]
class CharacterListCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Все ваши персонажи';
    const ARG_CHARACTER_LIST = 'Character List';
    const MESSAGE_ID = 'messageId';
    private array $buttons = [
        'inline_keyboard' => [
            [
                [
                'text' => 'Создать персонажа',
                'callback_data' => '/new'
                ]
            ]
        ]
    ];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ')
            ->AddArgument(self::ARG_CHARACTER_LIST, InputArgument::REQUIRED, 'Character list')
            ->AddArgument(self::MESSAGE_ID, InputArgument::OPTIONAL, 'Message Id ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);
        $messageId = $input->getArgument(self::MESSAGE_ID);

        $characters = json_decode($input->getArgument(self::ARG_CHARACTER_LIST), true);

        $ch_buttons = [];
        foreach ($characters as $character) {
            $ch_buttons[] = [[
                'text' => "Имя: ".$character['name']." Уровень: ".$character['level'],
                'callback_data' => json_encode(['CharacterView' => ['id' => $character['id']]])
            ]];
        }

        $this->buttons['inline_keyboard'] = $ch_buttons;

        $message = self::MESSAGE_START;

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            if ($messageId) {
                $this->sender->editMessageText(
                    $chatId,
                    $messageId,
                    $message,
                    $this->buttons
                );
            } else {
                $this->sender->sendMessage(
                    $chatId,
                    $message,
                    $this->buttons
                );
            }
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
