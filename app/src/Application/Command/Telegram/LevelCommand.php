<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:level',
    description: 'setLevel'
)]
class LevelCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Создание персонажа.';
    const ARG_BLANK = 'blank';
    const MESSAGE_ID = 'messageId';
    private array $buttons = [];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ')
            ->AddArgument(self::ARG_BLANK, InputArgument::REQUIRED, 'Болванка персонажа')
            ->AddArgument(self::MESSAGE_ID, InputArgument::OPTIONAL, 'Редактируемое сообщение');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);
        $messageId = $input->getArgument(self::MESSAGE_ID);
        $blank = json_decode($input->getArgument(self::ARG_BLANK), true);

        $level = [
            'blank' => [
                'level' => '1',
            ],
        ];

        $buttons = [
            'inline_keyboard' => [
                [
                    [
                        'text' => '1',
                        'callback_data' => json_encode($level)
                    ]
                ]
            ]
        ];

        $genders = [
            'male' =>'Мужской',
            'female' => 'Женский'
        ];

        $message = self::MESSAGE_START."\n 
Имя: {$blank['name']}\n 
Пол: ".(isset($blank['gender']) ? $genders[$blank['gender']] : '')."\n 
Уровень: {$blank['level']}";

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            if ($messageId) {
                $this->sender->editMessageText(
                    $chatId,
                    $messageId,
                    $message,
                    $buttons
                );
            } else {
                $this->sender->sendMessage(
                    $chatId,
                    $message,
                    $buttons
                );
            }
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
