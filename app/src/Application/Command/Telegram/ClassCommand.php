<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:class',
    description: '/character'
)]
class ClassCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Выбор класса:';
    const ARG_CHARACTER_ID = 'characterId';
    const ARG_CLASS_LIST = 'classList';
    const MESSAGE_ID = 'messageId';
    private array $buttons = [
        'inline_keyboard' => [
            []
        ]
    ];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ')
            ->AddArgument(self::ARG_CHARACTER_ID, InputArgument::REQUIRED, 'Character Id ')
            ->AddArgument(self::ARG_CLASS_LIST, InputArgument::REQUIRED, 'Class list ')
            ->AddArgument(self::MESSAGE_ID, InputArgument::OPTIONAL, 'Message id ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);
        $messageId = $input->getArgument(self::MESSAGE_ID);
        $classList = json_decode($input->getArgument(self::ARG_CLASS_LIST), true);
        $characterId = $input->getArgument(self::ARG_CHARACTER_ID);

        foreach ($classList as $class) {
            $this->buttons['inline_keyboard'][] = [[
                'text' => $class['name'],
                'callback_data' => json_encode([
                    'character' =>
                        [
                            'class' => $class['id'],
                            'character_id' => $characterId,
                        ]
                    ])
            ]];
        }
        $this->buttons['inline_keyboard'][] = [['text' => 'К списку персонажей', 'callback_data' => "CharacterList"]];

        $message = self::MESSAGE_START;

//        $message.= "\nИмя: ".$character['name'];
//        $message.= "\nРаса: ".$character['class'] ?: '';
//        $message.= "\nКасс: ".$character['class'] ?: '';
//        $message.= "\nУровень: ".$character['level'] ?: '';

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            if ($messageId) {
                $this->sender->editMessageText(
                    $chatId,
                    $messageId,
                    $message,
                    $this->buttons
                );
            } else {
                $this->sender->sendMessage(
                    $chatId,
                    $message,
                    $this->buttons
                );
            }
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
