<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:name',
    description: 'Введите имя:'
)]
class NameCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Введите имя:';
    private array $buttons = [];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            $this->sender->sendMessage(
                $chatId,
                self::MESSAGE_START,
                $this->buttons
            );
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
