<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:character',
    description: '/character'
)]
class CharacterCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Подробнее о персонаже:';
    const ARG_CHARACTER = 'character';
    const MESSAGE_ID = 'messageId';
    private array $buttons = [
        'inline_keyboard' => [
            [
                [
                    'text' => 'Установить характеристики',
                    'callback_data' => 'setCharacterAbility'
                ],
                [
                    'text' => 'Выбрать расу',
                    'callback_data' => 'setCharacterRace'
                ],
                [
                    'text' => 'Выбрать класс',
                    'callback_data' => 'setCharacterClass'
                ]
            ]
        ]
    ];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ')
            ->AddArgument(self::ARG_CHARACTER, InputArgument::REQUIRED, 'Message Id ')
            ->AddArgument(self::MESSAGE_ID, InputArgument::OPTIONAL, 'Character List ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);
        $messageId = $input->getArgument(self::MESSAGE_ID);
        $character = json_decode($input->getArgument(self::ARG_CHARACTER), true);

        $buttons = [
            'inline_keyboard' => [
                [
                    [
                        'text' => 'Выбрать расу',
                        'callback_data' => json_encode(['setCharacterRace'=>[
                            'characterId' => $character['id']
                        ]])
                    ],
                    [
                        'text' => 'Выбрать класс',
                        'callback_data' => json_encode(['setCharacterClass'=>[
                            'characterId' => $character['id']
                        ]])
                    ],
                    [
                        'text' => 'Характеристики',
                        'callback_data' => json_encode(['setCharacterAbility'=>[
                            'characterId' => $character['id']
                        ]])
                    ]],
                [
                    [
                        'text' => 'К списку',
                        'callback_data' => "CharacterList"
                    ]
                ]
            ]
        ];

        $message = self::MESSAGE_START; //todo сделать вывод основного про юзера

        $message.= "\nИмя: ".$character['name'];
        $message.= "\nРаса: ".(isset($character['race']) ? $character['race'] : '');
        $message.= "\nКасс: ".(isset($character['class']) ? $character['class'] : '');
        $message.= "\nУровень: ".(isset($character['level']) ? $character['level'] : '');

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            if ($messageId) {
                $this->sender->editMessageText(
                    $chatId,
                    $messageId,
                    $message,
                    $buttons
                );
            } else {
                $this->sender->sendMessage(
                    $chatId,
                    $message,
                    $buttons
                );
            }
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
