<?php

namespace App\Application\Command\Telegram;

use App\Application\Service\TelegramSender;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:telegram:command:invalid',
    description: '/invalid'
)]
class InvalidCommand extends Command
{
    const ARG_CHAT_ID = 'chatId';
    const MESSAGE_START = 'Неверная команда';
    private array $buttons = [
        'inline_keyboard' => [
            [
                [
                    'text' => 'Создать персонажа',
                    'callback_data' => '/new'
                ]
            ],
            [
                [
                    'text' => 'Список персонажей',
                    'callback_data' => '/list'
                ]
            ]
        ]
    ];

    public function __construct(private TelegramSender $sender)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'simulate',
                null,
                InputOption::VALUE_OPTIONAL,
                'Симулировать отправку, не отправлять по-настоящему'
            )
            ->AddArgument(self::ARG_CHAT_ID, InputArgument::REQUIRED, 'Telegram chatId ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        return Command::SUCCESS;
        $simulate = $input->getOption('simulate');
        $chatId = $input->getArgument(self::ARG_CHAT_ID);

        $buttons = $this->buttons;

        $message = self::MESSAGE_START;

        if ($simulate) {
            $this->sender->setOutput($output);
        }

        try {
            $this->sender->sendMessage(
                $chatId,
                $message,
                $buttons
            );
        } catch (Throwable $e) {
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
