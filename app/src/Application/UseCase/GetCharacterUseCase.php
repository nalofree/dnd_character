<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\GetCharacterRequest;
use App\Application\UseCase\DTO\Response\GetCharacterResponse;
use App\Domain\Repository\CharacterRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetCharacterUseCase
{
    public function __construct(
        private readonly CharacterRepositoryInterface $characterRepository
    ) {
    }

    public function __invoke(GetCharacterRequest $request): GetCharacterResponse
    {
        $character = $this->characterRepository->find($request->id);
        if (!$character) {
            throw new NotFoundHttpException('Раса не найдена');
        }
        dump($character);
        dump($character->getAbilityScores());
        return new GetCharacterResponse(
            $character->getName()->getValue(),
            $character->getAbilityScores() == null ? $character->getAbilityScores()->getValue() : [],
        );
    }
}
