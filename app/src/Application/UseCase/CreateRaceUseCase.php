<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\CreateRaceRequest;
use App\Application\UseCase\DTO\Response\CreateRaceResponse;
use App\Domain\Entity\Race;
use App\Domain\Repository\RaceRepositoryInterface;
use App\Domain\ValueObject\Description;
use App\Domain\ValueObject\Name;
use App\Domain\ValueObject\Race\BonusAbility;
use App\Domain\ValueObject\Race\FeatureList;
use App\Domain\ValueObject\Race\LanguageList;
use App\Domain\ValueObject\Race\NameList;
use App\Domain\ValueObject\Race\Speed;

class CreateRaceUseCase
{
    public function __construct(
        private readonly RaceRepositoryInterface $raceRepository
    ) {
    }

    public function __invoke(CreateRaceRequest $request): CreateRaceResponse
    {
        $race = new Race(
            new Name($request->name),
            new Description($request->description),
            new NameList($request->male_names),
            new NameList($request->female_names),
            new LanguageList($request->languages),
            new FeatureList($request->features),
            new BonusAbility($request->bonusAbility),
            new Speed($request->speed),
        );

        $this->raceRepository->save($race);

        return new CreateRaceResponse(
            $race->getId()
        );
    }
}
