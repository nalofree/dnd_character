<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\CreateCharacterRequest;
use App\Application\UseCase\DTO\Response\CreateCharacterResponse;
use App\Domain\Entity\Character;
use App\Domain\Enumeration\Gender;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\ValueObject\Character\Level;
use App\Domain\ValueObject\Name;

class CreateCharacterUseCase
{
    public function __construct(
        private readonly CharacterRepositoryInterface $characterRepository
    ) {
    }
    public function __invoke(CreateCharacterRequest $request): CreateCharacterResponse
    {
        $character = new Character(
            new Name($request->name),
            new Level($request->level),
            Gender::tryFrom($request->gender)
        );

        $this->characterRepository->save($character);

        return new CreateCharacterResponse(
            $character->getId()
        );
    }
}
