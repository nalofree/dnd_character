<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\SetCharacterAbilityScoresRequest;
use App\Application\UseCase\DTO\Response\SetCharacterAbilityScoresResponse;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\ValueObject\Character\AbilityScores;

class SetCharacterAbilityScoresUseCase
{
    public function __construct(
        private readonly CharacterRepositoryInterface $characterRepository
    ) {
    }
    public function __invoke(SetCharacterAbilityScoresRequest $request): SetCharacterAbilityScoresResponse
    {
        $character = $this->characterRepository->findBy(['id' => $request->characterId]);
        $character->setAbilityScores(new AbilityScores($request->abilityScores));

        $this->characterRepository->save($character);

        return new SetCharacterAbilityScoresResponse(
            $character->getAbilityScores()->getValue(),
            $character->getId()
        );
    }
}
