<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\SetCharacterStrengthRequest;
use App\Application\UseCase\DTO\Response\SetCharacterStrengthResponse;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\ValueObject\Character\StrengthScore;

class SetCharacterStrengthUseCase
{
    public function __construct(
        private readonly CharacterRepositoryInterface $characterRepository
    ) {
    }

    public function __invoke(SetCharacterStrengthRequest $request): SetCharacterStrengthResponse
    {
        $character = $this->characterRepository->findBy(['id' => $request->characterId]);
        $character->setStrength(new StrengthScore($request->strengthScore));

        $this->characterRepository->save($character);

        return new SetCharacterStrengthResponse(
            $character->getStrength()->getValue()
        );
    }
}
