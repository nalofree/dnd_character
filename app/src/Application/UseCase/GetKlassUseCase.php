<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\GetKlassRequest;
use App\Application\UseCase\DTO\Response\GetKlassResponse;
use App\Domain\Repository\KlassRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetKlassUseCase
{
    public function __construct(
        private readonly KlassRepositoryInterface $klassRepository
    ) {
    }

    public function __invoke(GetKlassRequest $request): GetKlassResponse
    {
        $klass = $this->klassRepository->find($request->id);
        if (!$klass) {
            throw new NotFoundHttpException('Раса не найдена');
        }
        return new GetKlassResponse(
            $klass->getName()->getValue(),
            $klass->getDescription()->getValue(),
            $klass->getHitBound()->getValue(),
            $klass->getArmor()->getValue(),
            $klass->getWeapon()->getValue(),
            $klass->getSavingTrows()->getValue()
        );
    }
}
