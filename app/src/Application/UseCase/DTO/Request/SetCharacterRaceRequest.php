<?php

namespace App\Application\UseCase\DTO\Request;

class SetCharacterRaceRequest
{
    public function __construct(
        public readonly int $raceId,
        public readonly int $characterId
    ) {
    }
}
