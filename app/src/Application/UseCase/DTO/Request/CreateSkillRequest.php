<?php

namespace App\Application\UseCase\DTO\Request;

class CreateSkillRequest
{
    public function __construct(
        public readonly string $name,
        public readonly string $ability
    ) {
    }
}
