<?php

namespace App\Application\UseCase\DTO\Request;

class SetCharacterStrengthRequest
{
    public function __construct(
        public readonly int $strengthScore,
        public readonly int $characterId
    ) {
    }
}
