<?php

namespace App\Application\UseCase\DTO\Request;

class CreateKlassRequest
{
    public function __construct(
        public readonly string $name,
        public readonly string $description,
        public readonly array $armor,
        public readonly string $hitBound,
        public readonly int $maxHits,
        public readonly array $savingTrows,
        public readonly array $tools,
        public readonly array $weapon,
        public readonly array $skills,
        public readonly int $skillCount,
        public readonly array $spells,
        public readonly array $incantations,
        public readonly array $abilityPriority,
        public readonly string $background
    ) {
    }
}
