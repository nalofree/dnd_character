<?php

namespace App\Application\UseCase\DTO\Request;

class GetCharacterRequest
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
