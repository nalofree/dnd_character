<?php

namespace App\Application\UseCase\DTO\Request;

class CreateCharacterRequest
{
    public function __construct(
        public readonly string $name,
        public readonly int    $level,
        public readonly string $gender
    ) {
    }
}
