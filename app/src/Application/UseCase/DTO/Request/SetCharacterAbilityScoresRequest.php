<?php

namespace App\Application\UseCase\DTO\Request;

class SetCharacterAbilityScoresRequest
{
    public function __construct(
        public readonly array $abilityScores,
        public readonly int $characterId
    ) {
    }
}
