<?php

namespace App\Application\UseCase\DTO\Request;

class GetKlassRequest
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
