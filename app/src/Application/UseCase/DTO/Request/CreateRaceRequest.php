<?php

namespace App\Application\UseCase\DTO\Request;

class CreateRaceRequest
{
    public function __construct(
        public readonly string $name,
        public readonly string $description,
        public readonly array $male_names,
        public readonly array $female_names,
        public readonly array $languages,
        public readonly array $features,
        public readonly array $bonusAbility,
        public readonly int $speed
    ) {
    }
}
