<?php

namespace App\Application\UseCase\DTO\Request;

class GetRaceRequest
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
