<?php

namespace App\Application\UseCase\DTO\Request;

class SetCharacterKlassRequest
{
    public function __construct(
        public readonly int $klassId,
        public readonly int $characterId
    ) {
    }
}
