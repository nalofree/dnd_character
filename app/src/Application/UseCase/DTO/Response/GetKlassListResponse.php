<?php

namespace App\Application\UseCase\DTO\Response;

class GetKlassListResponse
{
    public function __construct(
        public readonly array $classList,
    ) {
    }
}
