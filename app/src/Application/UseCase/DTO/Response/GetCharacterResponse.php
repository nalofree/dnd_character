<?php

namespace App\Application\UseCase\DTO\Response;

class GetCharacterResponse
{
    public function __construct(
        public readonly string $name,
        public readonly array $ability
    ) {
    }
}
