<?php

namespace App\Application\UseCase\DTO\Response;

class CreateCharacterResponse
{

    public function __construct(
        public readonly int $id
    ) {
    }
}
