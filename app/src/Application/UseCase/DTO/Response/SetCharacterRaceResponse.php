<?php

namespace App\Application\UseCase\DTO\Response;

class SetCharacterRaceResponse
{
    public function __construct(
        public readonly array $raceName
    ) {
    }
}
