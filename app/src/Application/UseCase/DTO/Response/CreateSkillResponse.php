<?php

namespace App\Application\UseCase\DTO\Response;

class CreateSkillResponse
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
