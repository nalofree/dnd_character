<?php

namespace App\Application\UseCase\DTO\Response;

class CreateRaceResponse
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
