<?php

namespace App\Application\UseCase\DTO\Response;

class GetRaceListResponse
{
    public function __construct(
        public readonly array $raceList
    ) {
    }
}
