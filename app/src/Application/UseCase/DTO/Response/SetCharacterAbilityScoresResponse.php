<?php

namespace App\Application\UseCase\DTO\Response;

class SetCharacterAbilityScoresResponse
{
    public function __construct(
        public readonly array $abilityScores,
        public readonly int $characterId
    ) {
    }
}
