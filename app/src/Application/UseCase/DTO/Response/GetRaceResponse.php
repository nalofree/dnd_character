<?php

namespace App\Application\UseCase\DTO\Response;

class GetRaceResponse
{
    public function __construct(
        public readonly string $name,
        public readonly string $description,
        public readonly array $bonusAbility,
        public readonly array $features,
    ) {
    }
}
