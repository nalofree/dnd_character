<?php

namespace App\Application\UseCase\DTO\Response;

class SetCharacterStrengthResponse
{
    public function __construct(
        public readonly int $strengthScore
    ) {
    }
}
