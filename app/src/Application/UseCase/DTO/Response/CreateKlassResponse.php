<?php

namespace App\Application\UseCase\DTO\Response;

class CreateKlassResponse
{
    public function __construct(
        public readonly int $id
    ) {
    }
}
