<?php

namespace App\Application\UseCase\DTO\Response;

class GetKlassResponse
{
    public function __construct(
        public readonly string $name,
        public readonly string $description,
        public readonly string $hitBound,
        public readonly array $armor,
        public readonly array $weapon,
        public readonly array $savingTrows
    ) {
    }
}
