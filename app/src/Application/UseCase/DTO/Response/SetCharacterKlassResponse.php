<?php

namespace App\Application\UseCase\DTO\Response;

class SetCharacterKlassResponse
{
    public function __construct(
        public readonly array $abilityArray
    ) {
    }
}
