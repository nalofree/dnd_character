<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\SetCharacterRaceRequest;
use App\Application\UseCase\DTO\Response\SetCharacterRaceResponse;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\Repository\RaceRepositoryInterface;

class SetCharacterRaceUseCase
{
    public function __construct(
        private readonly RaceRepositoryInterface $classRepository,
        private readonly CharacterRepositoryInterface $characterRepository,
    ) {
    }

    public function __invoke(SetCharacterRaceRequest $request): SetCharacterRaceResponse
    {
        $class = $this->classRepository->findBy(['id' => $request->raceId]);
        $character = $this->characterRepository->findBy(['id' => $request->characterId]);

        $character->setRace($class);

        $this->characterRepository->save($character);

        return new SetCharacterRaceResponse(
            $class->getName()->getValue()
        );
    }
}
