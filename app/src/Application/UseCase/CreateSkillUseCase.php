<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\CreateSkillRequest;
use App\Application\UseCase\DTO\Response\CreateSkillResponse;
use App\Domain\Entity\Skill;
use App\Domain\Repository\SkillRepositoryInterface;
use App\Domain\ValueObject\Name;

class CreateSkillUseCase
{
    public function __construct(
        private readonly SkillRepositoryInterface $skillRepository
    ) {
    }

    public function __invoke(CreateSkillRequest $request): CreateSkillResponse
    {
        $skill = new Skill(
            new Name($request->name),
            $request->ability
        );

        $this->skillRepository->save($skill);

        return new CreateSkillResponse(
            $skill->getId()
        );
    }
}
