<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\GetRaceRequest;
use App\Application\UseCase\DTO\Response\GetRaceResponse;
use App\Domain\Repository\RaceRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetRaceUseCase
{
    public function __construct(
        private readonly RaceRepositoryInterface $raceRepository
    ) {
    }

    public function __invoke(GetRaceRequest $request): GetRaceResponse
    {
        $race = $this->raceRepository->find($request->id);
        if (!$race) {
            throw new NotFoundHttpException('Раса не найдена');
        }
        return new GetRaceResponse(
            $race->getName()->getValue(),
            $race->getDescription()->getValue(),
            $race->getBonusAbility()->getValue(),
            $race->getFeatures()->getValue()
        );
    }
}
