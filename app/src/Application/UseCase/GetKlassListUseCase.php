<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Response\GetKlassListResponse;
use App\Domain\Repository\KlassRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetKlassListUseCase
{
    public function __construct(
        private readonly KlassRepositoryInterface $klassRepository
    ) {
    }

    public function __invoke(): GetKlassListResponse
    {
        $klassArray = $this->klassRepository->findAll();
        if (empty($klassArray)) {
            throw new NotFoundHttpException('Список классов пуст');
        }
        $klassList = [];
        foreach ($klassArray as $klass) {
            $klassList[] = [
                $klass->getName()->getValue(),
                $klass->getid()
            ];
        }
        return new GetKlassListResponse(
            $klassList
        );
    }
}
