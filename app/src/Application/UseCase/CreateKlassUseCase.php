<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\CreateKlassRequest;
use App\Application\UseCase\DTO\Response\CreateKlassResponse;
use App\Domain\Entity\Klass;
use App\Domain\Repository\KlassRepositoryInterface;
use App\Domain\Repository\SkillRepositoryInterface;
use App\Domain\ValueObject\Description;
use App\Domain\ValueObject\Klass\AbilityPriority;
use App\Domain\ValueObject\Klass\Armor;
use App\Domain\ValueObject\Klass\Background;
use App\Domain\ValueObject\Klass\DefaultIncantations;
use App\Domain\ValueObject\Klass\DefaultSpells;
use App\Domain\ValueObject\Klass\HitBound;
use App\Domain\ValueObject\Klass\MaxHits;
use App\Domain\ValueObject\Klass\SavingTrows;
use App\Domain\ValueObject\Klass\SkillCount;
use App\Domain\ValueObject\Klass\Tools;
use App\Domain\ValueObject\Klass\Weapon;
use App\Domain\ValueObject\Name;
use Doctrine\Common\Collections\ArrayCollection;

class CreateKlassUseCase
{
    public function __construct(
        private readonly KlassRepositoryInterface $klassRepository,
        private readonly SkillRepositoryInterface $skillRepository,
    ) {
    }

    public function __invoke(CreateKlassRequest $request): CreateKlassResponse
    {
        $skills = $this->skillRepository->findBy(['id' => $request->skills]);

        $class = new Klass(
            new Name($request->name),
            new Description($request->description),
            new HitBound($request->hitBound),
            new MaxHits($request->maxHits),
            new Armor($request->armor),
            new Weapon($request->weapon),
            new Tools($request->tools),
            new SavingTrows($request->savingTrows),
            new ArrayCollection($skills),
            new SkillCount($request->skillCount),
            new DefaultSpells($request->spells),
            new DefaultIncantations($request->incantations),
            new AbilityPriority($request->abilityPriority),
            new Background($request->background)
        );

        $this->klassRepository->save($class);

        return new CreateKlassResponse(
            $class->getId()
        );
    }
}
