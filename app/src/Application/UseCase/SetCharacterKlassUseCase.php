<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Request\SetCharacterKlassRequest;
use App\Application\UseCase\DTO\Response\SetCharacterKlassResponse;
use App\Domain\Repository\CharacterRepositoryInterface;
use App\Domain\Repository\KlassRepositoryInterface;

class SetCharacterKlassUseCase
{
    public function __construct(
        private readonly KlassRepositoryInterface $classRepository,
        private readonly CharacterRepositoryInterface $characterRepository,
    ) {
    }

    public function __invoke(SetCharacterKlassRequest $request): SetCharacterKlassResponse
    {
        $class = $this->classRepository->findBy(['id' => $request->klassId]);
        $character = $this->characterRepository->findBy(['id' => $request->characterId]);

        $character->setKlass($class);

        $this->characterRepository->save($character);

        return new SetCharacterKlassResponse(
            $class->getName()->getValue()
        );
    }
}
