<?php

namespace App\Application\UseCase;

use App\Application\UseCase\DTO\Response\GetRaceListResponse;
use App\Domain\Repository\RaceRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetRaceListUseCase
{
    public function __construct(
        private readonly RaceRepositoryInterface $raceRepository
    ) {
    }

    public function __invoke(): GetRaceListResponse
    {
        $raceArray = $this->raceRepository->findAll();
        if (empty($raceArray)) {
            throw new NotFoundHttpException('Список расс пуст');
        }
        $raceList = [];
        foreach ($raceArray as $race) {
            $raceList[] = [
                $race->getName()->getValue(),
                $race->getid()
            ];
        }
        return new GetRaceListResponse(
            $raceList
        );
    }
}
