<?php

namespace App\Application\Service;

use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TelegramSender
{
    private ?OutputInterface $output = null;
    public function __construct(
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag,
        private HttpClientInterface $client
    ) {
    }

    public function setOutput(?OutputInterface $output): void
    {
        $this->output = $output;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function sendMessage(
        int $chatId,
        string $message,
        array $replyMarkup
    ): void {
        $telegramApiToken = $this->parameterBag->get('telegram_http_api_token');
        if (empty($telegramApiToken)) {
            $this->logger->critical('Telegram API token не задан. Сообщение не отправлено.', ['telegram']);
            throw new InvalidArgumentException('Telegram API token не задан. Сообщение не отправлено.');
        }
        if (empty($chatId)) {
            $this->logger->critical('chatId не задан. Сообщение не отправлено.', ['telegram']);
            throw new InvalidArgumentException('chatId не задан. Сообщение не отправлено.');
        }

        if ($this->output) {
            $this->logger->warning('Сообщение отправлено в терминал, не в чат.', ['telegram']);
            $this->output->writeln("{$chatId}: {$message}");
            return;
        }

        $response = $this->client->request(
            'POST',
            'https://api.telegram.org/bot'.$telegramApiToken.'/sendMessage',
            ['json' => [
                'chat_id' => $chatId,
                'text' => $message,
                'reply_markup' => empty($replyMarkup) ? '' : json_encode($replyMarkup)
            ]]
        );
        if ($response->getStatusCode() !== 200) {
            $this->logger->critical("Не удалось отправить сообщение, chat_id: {$chatId}", ['telegram_sender']);
            $responseArray = $response->getContent();
            if (is_array($responseArray)) {
                $this->logger->critical(json_encode($responseArray), ['telegram_sender']);
            }
            throw new Exception('Telegram API вернул ошибку. Сообщение не отправлено.');
        }
    }

    public function editMessageText(
        int $chatId,
        int $messageId,
        string $message,
        array $replyMarkup
    ): void {
        $telegramApiToken = $this->parameterBag->get('telegram_http_api_token');
        if (empty($telegramApiToken)) {
            $this->logger->critical('Telegram API token не задан. Сообщение не отправлено.', ['telegram']);
            throw new InvalidArgumentException('Telegram API token не задан. Сообщение не отправлено.');
        }
        if (empty($chatId)) {
            $this->logger->critical('chatId не задан. Сообщение не отправлено.', ['telegram']);
            throw new InvalidArgumentException('chatId не задан. Сообщение не отправлено.');
        }

        if ($this->output) {
            $this->logger->warning('Сообщение отправлено в терминал, не в чат.', ['telegram']);
            $this->output->writeln("{$chatId}: {$message}");
            return;
        }

        $response = $this->client->request(
            'POST',
            'https://api.telegram.org/bot'.$telegramApiToken.'/editMessageText',
            ['json' => [
                'chat_id' => $chatId,
                'message_id' => $messageId,
                'text' => $message,
                'reply_markup' => empty($replyMarkup) ? '' : json_encode($replyMarkup)
            ]]
        );
        if ($response->getStatusCode() !== 200) {
            $this->logger->critical("Не удалось отправить сообщение, chat_id: {$chatId}", ['telegram_sender']);
            $responseArray = $response->getContent();
            if (is_array($responseArray)) {
                $this->logger->critical(json_encode($responseArray), ['telegram_sender']);
            }
            throw new Exception('Telegram API вернул ошибку. Сообщение не отправлено.');
        }
    }
}
